'use strict';

const $ = function(selector) {
	const starts_w_num = parseInt(selector[1]);
	if (starts_w_num >= 0 || selector.indexOf('-') !== -1) {
		let start_index = selector.indexOf('#');
		let end_index = selector.indexOf(' ');
		if (end_index === -1)
			end_index = selector.indexOf('.');
		if (end_index === -1)
			end_index = selector.length;
		if (end_index !== -1 && start_index !== -1) {
			const my_id = selector.slice(
			start_index + 1, 
			end_index
			);
			try {
				return document.querySelector(`[id="${my_id}"]${selector.slice(end_index, selector.length)}`);
			} catch (err) {
				return null;
			}
		} else {
			return null;
		}
	} else {
		try {
			const list = document.querySelectorAll(selector);
			const list_length = list.length;
			if (list_length === 0) {
				return null;
			} else if (list_length === 1) {
				return list[0];
			} else {
				return list;
			}
		} catch (err) {
			return null;
		}
	}
};
let forward_folders = [];
let current_folder = 'root________';
let current_folder_title = '';
let previous_folder = 'root________';
let drag_to;
let sel_bkmrks = {};
let typed_tmp = "";
let ctrl_command = false;
let key_down_timer = null;

/* Run once after initial render */
browser.storage.local.get(['mb'], function(ret) {
	$('#home').onclick = ev => open_folder('root________');
	$('#back_btn').onclick = go_back;
	$('#forward_btn').onclick = go_forward;
	$('#search_field').oninput = typed;
	$('#search_field').onclick = handle_focus;
	$('#display_list, #display_list > li').oncontextmenu = ev => handle_context_menu(ev);

	/* Restoring previous position/folder */
	if (ret.mb && ret.mb.current_folder) {
		current_folder = ret.mb.current_folder;
		open_folder(current_folder);
	} else {
		/* Initial app start */
		open_folder(current_folder);
	}
});

/* Keyboard events */
window.addEventListener('keydown', ev => {
	if ($('#context_menu')) {
		/* Right click menu */
		switch(ev.code) {
			case 'AltLeft': /* alt or option key to close right click menu */
			case 'AltRight': /* alt or option key to close right click menu */
			case 'Escape': { /* escape key to close right click menu */
				ev.preventDefault();
				hide_menus();
			} break;
			case 'ArrowUp': { /* up */
				ev.preventDefault();
				let btns = $('#context_menu').children;
				let k = -1;
				for (let i = btns.length - 1; i >= 0; i -= 1) {
					if (btns[i].classList.contains('selected')) {
						k = i;
						break;
					}
				}
				if (k === -1) {
					k = btns.length - 1;
					btns[k].classList.toggle('selected');
				} else {
					btns[k].classList.toggle('selected');
					k -= 1;
					if (k === -1) { k = btns.length - 1; }
					btns[k].classList.toggle('selected');
				}
			} break;
			case 'ArrowDown': { /* down */
				ev.preventDefault();
				let buttons = $('#context_menu').children;
				let c = -1;
				for (let i = 0, n = buttons.length; i < n; i+= 1) {
					if (buttons[i].classList.contains('selected')) {
						c = i;
						break;
					}
				}
				if (c === -1) {
					buttons[0].classList.toggle('selected');
				} else {
					buttons[c].classList.toggle('selected');
					c += 1;
					if (c === buttons.length) { c = 0; }
					buttons[c].classList.toggle('selected');
				}
			} break;
			case 'Enter': { /* enter */
				ev.preventDefault();
				$('#context_menu > .selected').click();
			} break;
		}
	} else if ($('#modal')) {
		/* Prompt menu */
		switch(ev.code) {
			case 'ArrowUp': { /* up */
				ev.preventDefault();
				let objcs = $('#modal input, #modal button');
				let c = -1;
				for (let i = objcs.length - 1; i >= 0; i-= 1) {
					if (objcs[i] === document.activeElement) {
						c = i;
						break;
					}
				}
				if (c === -1) {
					c = objcs.length - 1;
					objcs[c].focus();
				} else {
					c -= 1;
					if (c === -1) { c = objcs.length - 1; }
					objcs[c].focus();
				}
			} break;
			case 'ArrowDown': { /* down */
				ev.preventDefault();
				let objs = $('#modal input, #modal button');
				let f = -1;
				for (let i = 0, n = objs.length; i < n; i+= 1) {
					if (objs[i] === document.activeElement) {
						f = i;
						break;
					}
				}
				if (f === -1) {
					objs[0].focus();
				} else {
					f += 1;
					if (f === objs.length) { f = 0; }
					objs[f].focus();
				}
			} break;
		}
	} else {
		/* Bookmark list */
		switch(ev.code) {
			case 'ArrowUp': { /* up */
				ev.preventDefault();
				$('#search_field').blur();

				let selected = $('li.context.selected');

				if (selected) {
					let prev = null;
						try { prev = selected.previousSibling } catch {}
					let prev_parent = null;
						try { prev_parent = selected.closest('ul').closest('li') } catch {}

					selected.classList.toggle('selected');

					if (prev &&
						prev.querySelector('i.fa-caret-down') &&
						prev.querySelector('ul').hasChildNodes()) {

						prev.querySelector('ul > li:last-child').classList.toggle('selected');
						if ($('.selected').id && $('.selected').getBoundingClientRect().height <= 48) {
							$('.selected').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
						}
					} else if (prev) {
						prev.classList.toggle('selected');
						if ($('.selected').getBoundingClientRect().height <= 48) {
							$('.selected').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
						}
					} else if (prev_parent) {
						prev_parent.classList.toggle('selected');
						if ($('.selected').getBoundingClientRect().height <= 48) {
							$('.selected').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
						}
					} else {
						$('#search_field').focus();
						$('#search_field').select();
						$('#search_field').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
					}
				} else if ($('#display_list > li.context:last-child')) {
					$('#display_list > li.context:last-child').classList.toggle('selected');
					$('#display_list > li.context:last-child').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
				}
			} break;
			case 'ArrowDown': { /* down */
				ev.preventDefault();
				$('#search_field').blur();
				let selected = $('li.context.selected');
				if (selected) {
					let next = null;
						try { next = selected.nextSibling } catch {}
					let next_parent = null;
						try { next_parent = selected.closest('ul').closest('li').nextSibling } catch {}

					selected.classList.toggle('selected');

					if (selected.querySelector('i.fa-caret-down') && selected.querySelector('ul').hasChildNodes()) {
						selected.querySelector('ul > li:first-child').classList.toggle('selected');
						if ($('.selected').getBoundingClientRect().height <= 48) {
							$('.selected').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
						}
					} else if (next) {
						next.classList.toggle('selected');
						if ($('.selected').getBoundingClientRect().height <= 48) {
							$('.selected').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
						}
					} else if (next_parent) {
						next_parent.classList.toggle('selected');
						if ($('.selected').getBoundingClientRect().height <= 48) {
							$('.selected').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
						}
					} else {
						$('#search_field').focus();
						$('#search_field').select();
						$('#search_field').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
					}
				} else if ($('#display_list > li.context:first-child')) {
					$('#display_list > li.context:first-child').classList.toggle('selected');
					$('#display_list > li.context:first-child').scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
				}
			} break;
			case 'Backspace': { /* backspace - go back */
				if (document.activeElement.id !== 'search_field' || $('#search_field').value === '') {
                	ev.preventDefault();
                    $('#back_btn').click();
                }
 			} break;
			case 'ArrowLeft': { /* left - collapse subtree */
				if (document.activeElement.id !== 'search_field' || $('#search_field').value === '') {
					ev.preventDefault();
					let selected = $('li.context.selected');
					if (selected && selected.querySelector('i.fa-caret-down')) {
						selected.querySelector('i.fa-caret-down').click();
					} else if (selected && selected.closest('ul').closest('li')) {
						selected.closest('ul').closest('li').querySelector('i.fa-caret-down').click();
					}
				}
			} break;
			case 'ArrowRight': { /* right - expand subtree */
				if (document.activeElement.id !== 'search_field' || $('#search_field').value === '') {
					ev.preventDefault();
					let selected = $('li.context.selected');
					if (selected && selected.querySelector('i.fa-caret-right')) {
						selected.querySelector('i.fa-caret-right').click();
					}
				}
			} break;
			case 'PageUp': { /* page up - scroll up one screen */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					scrollBy({top: -616, left: 0, behavior: 'smooth'});
				}
			} break;
			case 'PageDown': { /* page down - scroll down one screen */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					scrollBy({top: 616, left: 0, behavior: 'smooth'});
				}
			} break;
			case 'Home': { /* home - select first button */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					let selected = $('li.context.selected');
					if (selected) {
						selected.classList.toggle('selected');
					}
					const li_buttons = $('li.context');
					li_buttons[0].classList.toggle('selected');
					li_buttons[0].scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
				}
			} break;
			case 'End': { /* end - select last button */
				if (document.activeElement.id !== 'search_field') {
					$('#search_field').blur();
					let selected = $('li.context.selected');
					if (selected) {
						selected.classList.toggle('selected');
					}
					const li_buttons = $('li.context');
					li_buttons[li_buttons.length - 1].classList.toggle('selected');
					li_buttons[li_buttons.length - 1].scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
				}
			} break;
			case 'ControlRight':
			case 'ControlLeft':
			case 'MetaRight': /* command */
			case 'MetaLeft': { /* command */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					ctrl_command = true;
				}
			} break;
			case 'Delete': { /* delete */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					const selected = $('li.context.selected');
					if (selected) {
						browser.bookmarks.get(selected.id, function(obj) {
							ev.target.id = 'context_del';
							handle_modal(ev, selected.id);
							$('#modal_in_title').value = obj[0].title;
							if ($('#modal_in_url')) {
								$('#modal_in_url').value = obj[0].url;
							}
							let key_count = Object.keys(sel_bkmrks).length;
							if (key_count) {
								$('#modal_in_title').value = `Delete ${key_count}`;
								$('#modal_submit').innerText = 'Delete';
								$('#modal_submit').onclick = ev => delete_bookmark(sel_bkmrks);
							} else {
								$('#modal_in_title').value = obj[0].title;
								$('#modal_submit').innerText = 'Delete';
								$('#modal_submit').onclick = ev => delete_bookmark(selected.id);
							}
						});
					}
				}
			} break;
			case 'Slash': { /* slash key */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					$('#search_field').focus();
					$('#search_field').select();
					if ($('li.context.selected')) {
						$('li.context.selected').classList.toggle('selected');
					}
				}
			} break;
			case 'Space': { /* space key to select a bookmark or a folder */
				if (document.activeElement.id !== 'search_field') {
					ev.preventDefault();
					let selected = $('li.context.selected').id;
					select_multi(selected);
				}
			} break;
			case 'Enter': { /* enter */
				ev.preventDefault();
				$('li.context.selected > span').click();
			} break;
			case 'AltRight': /* alt or option key to open right click menu */
			case 'AltLeft': { /* alt or option key to open right click menu */
				ev.preventDefault();
				handle_context_menu(ev, true);
			} break;
			default: {
				if (document.activeElement.tagName !== 'INPUT') {
					ev.preventDefault();
					/* select buttons via typing on a keyboard */
					let k = ev.key.toLowerCase();
					if (k.length === 1 && k.match(/[a-z0-9_\s]/)) {
						typed_tmp += k;
						if (typed_tmp.length >= 2) {
							clearTimeout(key_down_timer); 
							key_down_timer = setTimeout(function() {
								let found_id;
								for (let i = 0, n = $('li').length; i < n; i += 1) {
									let title_lower = $('li')[i].querySelector('span').innerText.toLowerCase();
									let exists = title_lower.indexOf(typed_tmp);
									if (exists > -1) {
										found_id = $('li')[i].id;
										break;
									}
								}
								if (found_id) {
									let button = document.getElementById(found_id);
									button.scrollIntoView({block: 'center', inline: 'end', behavior: 'smooth'});
									$('li.context.selected').classList.toggle('selected');
									button.classList.toggle('selected');
								}
								ctrl_command = false;
								typed_tmp = "";
							}, 300);
						/* ctrl + a / command + a to select all */
						} else if (ctrl_command === true && typed_tmp === 'a') {
							clearTimeout(key_down_timer); 
							key_down_timer = setTimeout(function() {
								for (let i = 0, n = $('.context').length; i < n; i += 1) {
									let focused_id = $('.context')[i].id;
									select_multi(focused_id);
								}
								ctrl_command = false;
								typed_tmp = "";
							}, 300);
						}
					}
				}
			} break;
		}
	}
});

function handle_dragstart(ev) {
	ev.dataTransfer.setData("Text", ev.target.id);
}

function handle_dragover(ev) {
	ev.preventDefault();
	if (ev.target.closest('li').classList.contains('folder')) {
		drag_to = ev.target.closest('li').id;
	} else if (ev.target.closest('li').classList.contains('link')) {
		drag_to = ev.target.closest('ul').closest('li').id || current_folder;
	}
}

function handle_drop(ev) {
	ev.preventDefault();
	const id = ev.dataTransfer.getData("Text");
	if (id && drag_to && drag_to.length) {
		move_bookmarks(id, drag_to);
	}
}

/* Selects text when search field in focus */
function handle_focus() {
	$('#search_field').select();
}

function hide_menus() {
	if ($('#context_background'))
		$('#context_background').remove();
	if ($('#modal'))
		$('#modal').remove();
	if ($('#context_menu'))
		$('#context_menu').remove();
}

function compare_titles(a, b) {
	if (a.title && b.title) { 
	    let x = a.title.toLowerCase();
	    let y = b.title.toLowerCase();
	    if (x < y) return -1;
	    if (x > y) return 1;
	    return 0;
	}
}

/* Folder click */
function open_folder(id) {
if (id) {
	current_folder = id;
	browser.bookmarks.getSubTree(id, function(obj) {
		current_folder_title = obj[0].title;
		const subarr = obj[0].children;
		if (subarr) {
			let display_list = $('#display_list');
			previous_folder = obj[0].parentId;
			make_buttons(subarr, display_list);
			browser.storage.local.set({'mb': {current_folder: id} });
			if (forward_folders.length && $(`#${forward_folders[forward_folders.length - 1]}`)) {
				$(`#${forward_folders[forward_folders.length - 1]}`).classList.toggle('selected');
			}
			if (document.querySelectorAll('li.context.selected').length === 0) {
				try {document.querySelector('li.context').classList.toggle('selected')} catch {}
			}
		}
	});
}
}

/*
 list: Object from browser.bookmarks.get
 obj: DOM object
 Replaces buttons in a destination object
*/
function make_buttons(list, obj) {
	let all_buttons = [];
	let folder_arr = [];
	let link_arr = [];
	if (list) {
		for (let i = 0, n = list.length; i < n; i += 1) {
			const ff = list[i];
			let title_final = ff.title;
			if (ff.parent_title) {
				title_final = `${ff.title} « ${ff.parent_title}`;
			}
			if (ff.children) {
				let folder = document.createElement('li');
					folder.ondragstart = ev => handle_dragstart(ev);
					folder.ondragover = ev => handle_dragover(ev);
					folder.ondrop = ev => handle_drop(ev);
					folder.id = ff.id;
					folder.classList.add('context');
					folder.classList.add('folder');
					folder.draggable = true;
				let caret_right = document.createElement('i');
					caret_right.onclick = ev => exp_col(ev);
					caret_right.classList.add('fas');
					caret_right.classList.add('fa-caret-right');
				let ico = document.createElement('i');
					ico.onclick = ev => select_multi(ff.id);
					ico.classList.add('fas');
					ico.classList.add('fa-folder');
				let title = document.createElement('span');
					title.onclick = ev => {
						open_folder(ff.id);
						browser.permissions.request({origins: ['https://icons.duckduckgo.com/*']});
					};
					title.textContent = title_final;
				let sub_list = document.createElement('ul');
					sub_list.classList.add('expand');
				folder.appendChild(caret_right);
				folder.appendChild(ico);
				folder.appendChild(title);
				folder.appendChild(sub_list);

				folder_arr.push({title: title_final, content: folder});
			} else if (ff.url.indexOf('data:') !== 0 && ff.url.indexOf('place:') !== 0) {
				set_ico(ff.url, ff.id);
				let link = document.createElement('li');
					link.ondragstart = ev => handle_dragstart(ev);
					link.ondragover = ev => handle_dragover(ev);
					link.ondrop = ev => handle_drop(ev);
					link.id = ff.id;
					link.classList.add('context');
					link.classList.add('link');
					link.draggable = true;
				let ico = document.createElement('i');
					ico.onclick = ev => select_multi(ff.id);
					ico.classList.add('fas');
					ico.classList.add('fa-star');
				let img = document.createElement('img');
				img.style.display = 'none';
				let title = document.createElement('span');
					title.onclick = ev => new_tab(ff.url);
					title.textContent = title_final;
				let br = document.createElement('br');
				let url = document.createElement('small');
					url.textContent = ff.url;
				link.appendChild(ico);
				ico.appendChild(img);
				link.appendChild(title);
				link.appendChild(br);
				link.appendChild(url);

				link_arr.push({title: title_final, content: link});
			}
		};

		folder_arr.sort(compare_titles);
		link_arr.sort(compare_titles);
		all_buttons = folder_arr.concat(link_arr);
		obj.replaceChildren();
		for (let i = 0, n = all_buttons.length; i < n; i += 1) {
			obj.appendChild(all_buttons[i].content);
		}
	}
}

/* Expand or collapse folder list */
function exp_col(ev) {
	let triangle = ev.target;
	let id = triangle.parentElement.id;
	let expand = triangle.nextSibling.nextSibling.nextSibling;
	if (id) {
		browser.bookmarks.getSubTree(id, function(obj) {
			let subarr = obj[0].children;
			if (subarr) {
				triangle.classList.toggle('fa-caret-right');
				triangle.classList.toggle('fa-caret-down');
				let selected_btn = $('li.context.selected');
				if (triangle.classList.contains('fa-caret-down')) {
					make_buttons(subarr, expand);
					expand.style.display = 'block';
					if (selected_btn && expand.children.length) {
						selected_btn.classList.toggle('selected');
						expand.children[0].classList.toggle('selected');
					}
				} else if (triangle.parentElement.classList.contains('selected')) {
					expand.style.display = 'none';
					expand.replaceChildren();
				} else {
					if (selected_btn && selected_btn.closest('ul').closest('li')) {
						selected_btn.closest('ul').closest('li').classList.toggle('selected');
					}
					expand.style.display = 'none';
					expand.replaceChildren();
				}
			}
		});
	}
}

function select_multi(id) {
if (id) {
	let type;

	if ($(`#${id}.folder`)) {
		type = 'folder';
	} else {
		type = 'link';
	}

	if ( ! sel_bkmrks[`${id}`]) {
		sel_bkmrks[`${id}`] = {'id': id, 'type': type};
	} else if (sel_bkmrks[id]) {
		type = sel_bkmrks[id].type;
		delete sel_bkmrks[id];
	} else {
		sel_bkmrks = {};
		sel_bkmrks[`${id}`] = {'id': id, 'type': type};
	}
	switch (type) {
		case 'link':
			if ($(`#${id} > i > img`).src.length > 0 && $(`#${id} > i > img`).style.display === 'inline-block') {
				$(`#${id} > i > img`).style.display = 'none';
				$(`#${id} > i`).classList.toggle('fa-check');
			} else if ($(`#${id} > i > img`).src.length > 0 && $(`#${id} > i > img`).style.display === 'none') {
				$(`#${id} > i > img`).style.display = 'inline-block';
				$(`#${id} > i`).classList.toggle('fa-check');
			} else {
				$(`#${id} > i`).classList.toggle('fa-star');
				$(`#${id} > i`).classList.toggle('fa-check');
			}
		break;
		case 'folder':
			$(`#${id} > i+i`).classList.toggle('fa-folder');
			$(`#${id} > i+i`).classList.toggle('fa-check');
		break;
	}
}}

/* Bookmark click */
function new_tab (url) {
	if (url && url.length) {
		browser.tabs.create({url: url});
	}
}

function go_back() {
	forward_folders.push(current_folder);
	if (previous_folder && previous_folder.length) {
		open_folder(previous_folder);
	}
}

function go_forward() {
	if (forward_folders && forward_folders.length) {
		open_folder(forward_folders.pop());
	}
}

function move_bookmarks(ids, to_id) {
	hide_menus();
	if ( ! to_id) {
		to_id = current_folder;
	}
	if (ids && Object.keys(ids).length && Object.prototype.toString.call(ids) === '[object Object]') {
		for (let key in ids) {
			if (ids[key].id === to_id) {break;}
			browser.bookmarks.move(ids[key].id, {parentId: to_id}, function() {
				if ($(`#${ids[key].id}`) &&
					$(`#${ids[key].id}`).closest('ul').closest('li').children[0].classList.contains('fa-caret-down')) {
						$(`#${ids[key].id}`).closest('ul').closest('li').children[0].click();
						$(`#${ids[key].id}`).closest('ul').closest('li').children[0].click();
				} else if ($(`#${ids[key].id}`) &&
					$(`#${ids[key].id}`).closest('#display_list')) {
						open_folder(current_folder);
				} else {
					open_folder(current_folder);
				}
				if ($(`#${to_id} > i:first-of-type`) &&
					$(`#${to_id} > i:first-of-type`).classList.contains('fa-caret-down')) {
						$(`#${to_id} > i:first-of-type`).click();
						$(`#${to_id} > i:first-of-type`).click();
				}
			});
		}
	} else if (ids && ids.length && Object.prototype.toString.call(ids) === '[object String]' && ids !== to_id) {
		browser.bookmarks.move(ids, {parentId: to_id}, function() {
			if ($(`#${ids}`) &&
				$(`#${ids}`).closest('ul').closest('li').children[0].classList.contains('fa-caret-down')) {
					$(`#${ids}`).closest('ul').closest('li').children[0].click();
					$(`#${ids}`).closest('ul').closest('li').children[0].click();
			} else if ($(`#${ids}`) &&
				$(`#${ids}`).closest('#display_list')) {
					open_folder(current_folder);
			} else {
				open_folder(current_folder);
			}
			if ($(`#${to_id} > i:first-of-type`) &&
				$(`#${to_id} > i:first-of-type`).classList.contains('fa-caret-down')) {
					$(`#${to_id} > i:first-of-type`).click();
					$(`#${to_id} > i:first-of-type`).click();
			}
		});
	}
	sel_bkmrks = {};
}

function delete_bookmark(id_or_obj) {
	hide_menus();
	if (id_or_obj &&
		Object.keys(id_or_obj).length &&
		Object.prototype.toString.call(id_or_obj) === '[object Object]') {

		for (let key in id_or_obj) {
			browser.bookmarks.getSubTree(id_or_obj[key].id, function(obj) {
				if (obj[0].children) {
					browser.bookmarks.removeTree(obj[0].id, function() {
						$(`#${obj[0].id}`).remove();
					});
				} else {
					browser.bookmarks.remove(id_or_obj[key].id, function() {
						$(`#${obj[0].id}`).remove();
					});
				}
				delete id_or_obj[key];
			});
		}
	} else if (id_or_obj && id_or_obj.length && Object.prototype.toString.call(id_or_obj) === '[object String]') {
		browser.bookmarks.getSubTree(id_or_obj, function(obj) {
			if (obj[0].children) {
				browser.bookmarks.removeTree(obj[0].id, function() {
					$(`#${obj[0].id}`).remove();
				});
			} else {
				browser.bookmarks.remove(obj[0].id, function() {
					$(`#${obj[0].id}`).remove();
				});
			}
		});
	}
}

/* {id: string, title: string, url: string} */
function edit_bookmark(id, title, url) {
	hide_menus();
	if (id && title && title.length && url && url.length) {
		browser.bookmarks.update(id, {title: title, url: url}, function() {
			$(`#${id} span`).innerText = title;
			$(`#${id} small`).innerText = url;
		});
	} else if (id && title && title.length) {
		browser.bookmarks.update(id, {title: title}, function() {
			$(`#${id} span`).innerText = title;
		});
	}
}

function create_fob(title, url, folder_id) {
	hide_menus();
	let new_fob = {};
	if ( ! folder_id || ! folder_id.length) {
		folder_id = current_folder;
	}
	if (title && title.length && url && url.length) {
		/* create bookmark */
		new_fob = {parentId: folder_id, title: title, url: url};
	} else if (title && title.length) {
		/* create folder */
		new_fob = {parentId: folder_id, title: title};
	}
	if (new_fob && Object.keys(new_fob).length) {
		browser.bookmarks.create(new_fob, function(info) {
			open_folder(folder_id);
		});
	}
}

function handle_modal(ev, id, folder_id) {
	ev.preventDefault();

	let t_modal = $('#t_modal').content.cloneNode(true);
	let list_rect = $('body').getBoundingClientRect();
	document.body.appendChild(t_modal);

	if (id && $(`#${id}`).classList.contains('folder')) {
		$('#modal_in_url').remove();
	} else {
		if (ev.target.id === 'context_create') {
			$('#modal_in_url').remove();
		} else if (ev.target.id === 'context_move') {
			$('#modal_in_url').remove();
		}
	} 

	let modal = $('#modal');
	let y_pos = null;

	if ($(`#${id}`)) {
		y_pos = $(`#${id}`).getBoundingClientRect().y + window.scrollY + 57;
	} else {
		y_pos = list_rect.height / 2;
	}

	if (y_pos > list_rect.height) {
		modal.style.top = String(y_pos - 86) + 'px';
	} else {
		modal.style.top = String(y_pos - 50) + 'px';
	}
	modal.style.left = String(list_rect.width / 2 - 96) + 'px';

	if ($('#context_menu')) {
		$('#context_menu').remove();
	}

	$('#modal_cancel').onclick = ev => hide_menus();

	/* if no ID then create and move only */
	if (ev.target.id === 'context_del') {
		browser.bookmarks.get(id, function(obj) {
			$('#modal_in_title').value = obj[0].title;
			if ($('#modal_in_url')) {
				$('#modal_in_url').value = obj[0].url;
			}
			let key_count = Object.keys(sel_bkmrks).length;
			if (key_count) {
				$('#modal_in_title').value = `Delete ${key_count}`;
				$('#modal_submit').innerText = 'Delete';
				$('#modal_submit').onclick = ev => delete_bookmark(sel_bkmrks);
			} else {
				$('#modal_in_title').value = obj[0].title;
				$('#modal_submit').innerText = 'Delete';
				$('#modal_submit').onclick = ev => delete_bookmark(id);
			}
		});
	} else if (ev.target.id === 'context_edit') {
		browser.bookmarks.get(id, function(obj) {
			$('#modal_in_title').value = obj[0].title;
			if ($('#modal_in_url')) {
				$('#modal_in_url').value = obj[0].url;
				$('#modal_submit').innerText = 'Save';
				$('#modal_submit').onclick = ev => edit_bookmark(id, $('#modal_in_title').value, $('#modal_in_url').value);
			} else {
				$('#modal_submit').innerText = 'Save';
				$('#modal_submit').onclick = ev => edit_bookmark(id, $('#modal_in_title').value, '');
			}
		});
	} else if (ev.target.id === 'context_create') {
		$('#modal_in_title').value = '';
		if ($('#modal_in_url')) {
			$('#modal_in_url').value = '';
		}
		$('#modal_submit').innerText = 'Create';
		$('#modal_submit').onclick = ev => create_fob($('#modal_in_title').value, undefined, folder_id);
	} else if (ev.target.id === 'context_move') {
		if (current_folder_title.length) {
			$('#modal_in_title').value = 'Move to ' + current_folder_title;
		} else {
			$('#modal_in_title').value = "Can't move to root";
		}
		$('#modal_submit').innerText = 'Move';
		$('#modal_submit').onclick = ev => move_bookmarks(sel_bkmrks, folder_id);
	}
}

/* Right click menu */
function handle_context_menu(ev, kbd = false) {
	ev.preventDefault();

	let id = '';
	let folder_id = '';

	if (kbd === true) {
		let selected = $('li.context.selected');

		if (selected &&
			selected.closest('ul').closest('li') &&
			selected.closest('ul').closest('li').querySelector('fa-caret-down')) {

			id = selected.id;
			folder_id = selected.closest('ul').closest('li').id;
		} else if (selected) {
			id = selected.id;
			folder_id = current_folder;
		} else {
			folder_id = current_folder;
		}
	} else if (ev.target.tagName === 'SPAN' || ev.target.tagName === 'SMALL') {
		if (ev.target.parentNode.parentNode.parentNode.querySelector('fa-caret-down')) {
			id = ev.target.parentNode.id;
			folder_id = ev.target.parentNode.parentNode.parentNode.id
		} else {
			id = ev.target.parentNode.id;
			folder_id = current_folder;
		}
	} else if (ev.target.tagName === 'LI') {
		id = ev.target.id;
		folder_id = current_folder;
	} else {
		folder_id = current_folder;
	}

	let t_context_background = $('#t_context_background').content.cloneNode(true);
	let t_context_menu = $('#t_context_menu').content.cloneNode(true);

	let list_rect = $('body').getBoundingClientRect();

	document.body.appendChild(t_context_background);
	document.body.appendChild(t_context_menu);

	let context_menu = $('#context_menu');

	let y_pos = null;
	let x_pos = null;

	if ($(`#${id}`)) {
		y_pos = $(`#${id}`).getBoundingClientRect().y + window.scrollY + 7;
		x_pos = $(`#${id}`).getBoundingClientRect().x + window.scrollX + 118;
	} else {
		y_pos = $('body').getBoundingClientRect().height / 2;
		x_pos = $('body').getBoundingClientRect().width / 2 - 57;
	}

	/* bottom adjustment */
	if (y_pos + 30 > list_rect.y + list_rect.height) {
		context_menu.style.top = String(y_pos - 30) + 'px';
	} else {
		context_menu.style.top = String(y_pos) + 'px';
	}
	/* right adjustment */
	if (x_pos + 150 > list_rect.x + list_rect.width) {
		if (x_pos - 150 < list_rect.x) {
			context_menu.style.left = String(list_rect.x) + 'px';
		} else {
			context_menu.style.left = String(x_pos - 150) + 'px';
		}
	} else {
		context_menu.style.left = String(x_pos) + 'px';
	}

	$('#context_del').onclick = ev => handle_modal(ev, id, folder_id);
	$('#context_edit').onclick = ev => handle_modal(ev, id, folder_id);
	$('#context_create').onclick = ev => handle_modal(ev, id, folder_id);
	$('#context_move').onclick = ev => handle_modal(ev, id, folder_id);
	$('#context_background').onclick = ev => hide_menus();

	if ($('#display_list').children.length === 0) {
		$('#context_del').remove();
		$('#context_edit').remove();
	}
}

/* Displaying search results */
function typed() {
	const search_field = $('#search_field');
	if (document.activeElement.id === 'search_field' && search_field.value.length >= 2) {
		clearTimeout(key_down_timer);
		key_down_timer = setTimeout(function() {
			browser.bookmarks.getTree(function(obj) {
				let subarr = obj[0].children;
				let found = [];
				let sought = search_field.value.toLowerCase();
				while (subarr.length) {
					let t = subarr.pop();					
					if (t.children) {
						for (let i = 0, n = t.children.length; i < n; i += 1) {
							t.children[i]['parent_title'] = t.title;
							subarr.push(t.children[i]);
						}
					}
					if (t.title.toLowerCase().indexOf(sought) > -1) {
						found.push(t);
					}
				}
				make_buttons(found, $('#display_list'));
			});
		}, 300);
	} else {
		open_folder(current_folder);
	}
}

/* Gets ico from duckduckgo and saves it to local storage. Returns promise with ico blob. */
function get_save_ico(domain) {
	return new Promise(resolve => {
		fetch(`https://icons.duckduckgo.com/ip3/${domain}.ico`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		})
		.then(function(response) {
			if (response.status === 200) {
				const reader = response.body.getReader();
				let data = new Uint8Array();
				reader.read().then(function read_stream({ done, value }) {
					if (done) {
						const blob = new Blob([data], {type: 'image/png'});
						if (blob.size > 4) {
							let reader = new FileReader();
							reader.readAsDataURL(blob);
							reader.onload = function() {
								browser.storage.local.set({[domain]: JSON.stringify({data: reader.result})});
								resolve(reader.result);
							};
						}
						return;
					} else {
						let new_chunk = new Uint8Array(data.length + value.length);
						new_chunk.set(data, 0);
						new_chunk.set(value, data.length);
						data = new_chunk;
						return reader.read().then(read_stream);
					}
				});
			} else {
				if (response.status === 404) {
					browser.storage.local.set({[domain]: JSON.stringify({data: '404'})});
				}
			}
		});
	});
}

/* Sets ico for a link from local storage or duckduckgo with timeout 1000ms. */
function set_ico(url, id) {
	const domain = new URL(url).hostname;
	browser.storage.local.get([domain]).then(mystorage => {
		if (mystorage[domain] && typeof mystorage[domain] === 'string') {
			const img_obj = JSON.parse(mystorage[domain]);
			if (img_obj.data.length > 4 && $(`#${id} > i > img`)) {
				$(`#${id} > i > img`).src = img_obj.data;
				$(`#${id} > i`).classList.toggle('fa-star');
				$(`#${id} > i > img`).style.display = 'inline-block';
			}
		} else {
			browser.permissions.getAll().then(perm => {
				if (perm.origins.indexOf('https://icons.duckduckgo.com/*') > -1) {
					setTimeout(() => {
						get_save_ico(domain).then(base64 => {
							if ($(`#${id} > i > img`)) {
								$(`#${id} > i > img`).src = base64;
								$(`#${id} > i`).classList.toggle('fa-star');
								$(`#${id} > i > img`).style.display = 'inline-block';
							}
						});
					}, 1000);
				}
			});
		}
	});
}