Private, secure, fast, no ads, open source bookmarks manager with dark and light styles activating automatically depending on system theme.  

Usage:  
To move one or multiple items click on icon so it shows a check mark, then right click when you inside a destination folder to "move here".  
Also, there is drag and drop functionality for single items.  

"/" - jump to search  
"option" or "alt" - open/close context menu  
"backspace" or "delete" on Mac - go back  
"delete" or "fn + delete" on Mac - delete bookmark or a folder  
"home" - select first item in the list  
"end" - select last item in the list  
"page up" - scroll up  
"page down" - scroll down  
"space" - select multiple bookmarks  
"escape" - unselect bookmarks  
